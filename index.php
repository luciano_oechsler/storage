<?php
/**
 *
 * Storage :: Gerenciamento de Banco de Dados
 *
 * @author	Luciano Melo
 * @version	01.10.2015
 *
 */
class Storage {

	/**
	 *
	 * Configuração da Conexão
	 *
	 */
	public $hostname = '';
	public $username = '';
	public $password = '';
	public $database = '';
	private $last;
	const LAST = 1;

	/**
	 *
	 * Smarty Connect
	 *
	 */
	public function __construct() {
		if(!empty($this->hostname) AND !empty($this->username) AND !empty($this->password)) {
			$this->connect();

			if(!empty($this->database)) {
				$this->select_db($this->database);
			}
		}
	}

	/**
	 *
	 * Abre uma conexão com um servidor MySQL
	 *
	 */
	public function connect() {
		if(!$this->connect = mysqli_connect($this->hostname, $this->username, $this->password)) {
			die('Erro ao conectar ao servidor MySQL: ' . mysqli_connect_error());
		}

		$this->query("SET character_set_results    = 'utf8'");
		$this->query("SET character_set_connection = 'utf8'");
		$this->query("SET character_set_database   = 'utf8'");
		$this->query("SET character_set_server     = 'utf8'");
	}

	/**
	 *
	 * Selecionar um banco de dados MySQL
	 *
	 */
	public function select_db() {
		if(!mysqli_select_db($this->connect, $this->database)) {
			die('Erro ao selecionar o banco de dados: ' . $this->connect->error);
		}
	}

	/**
	 *
	 * Enviar uma consulta MySQL
	 *
	 */
	public function query($value) {
		if($value == LAST) {
			if(!$this->query = mysqli_query($this->connect, $this->last)) {
				return $this;
			}
		}
		else {
			if(!$this->query = mysqli_query($this->connect, $value)) {
				die('Erro ao efetuar consulta: ' . mysqli_error($this->connect));
			}
		}
		$this->last = $value;
		return $this;
	}

	/**
	 *
	 * Insere um novo registro utilizando uma matriz associativa
	 *
	 */
	public function insert_array($table, $array) {
		foreach($array as $key => $value) {
			$columns[] = '`' . $key . '`';

			if($value != '') {
				$fields[] = "'" . $value . "'";
			}
			else {
				$fields[] = 'NULL';
			}
		}

		$columns = implode(',', $columns);
		$fields = implode(',', $fields);

		$this->query("INSERT INTO `" . $table . "` (" . $columns . ") VALUES (" . $fields . ");");
	}

	/**
	 *
	 * Obtérm uma linha como uma matriz númerica e associativa
	 *
	 */
	public function fetch_array() {
		while($array[] = $this->query->fetch_array(MYSQLI_BOTH));
			return array_filter($array);
	}

	/**
	 *
	 * Obtém uma linha como matriz associativa
	 *
	 */
	public function fetch_assoc() {
		while($array[] = $this->query->fetch_array(MYSQLI_ASSOC));
			return array_filter($array);
	}

	/**
	 *
	 * Obtém uma linha como matriz númerica
	 *
	 */
	public function fetch_row() {
		while($array[] = $this->query->fetch_array(MYSQLI_NUM));
			return array_filter($array);
	}

	/**
	 *
	 * Obtém o número de linhas em um resultado
	 *
	 */
	public function num_rows() {
		return mysqli_num_rows($this->query);
	}

	/**
	 *
	 * Obtém o número de campos em um resultado
	 *
	 */
	public function num_fields() {
		return mysqli_field_count($this->connect);
	}

	/**
	 *
	 * Obtém o número de linhas atingidas na operação anterior
	 *
	 */
	public function affected_rows() {
		return mysqli_affected_rows($this->connect);
	}

	/**
	 *
	 * Obtém o ID gerado pela operação INSERT anterior
	 *
	 */
	public function insert_id() {
		return mysqli_insert_id($this->connect);
	}

	/**
	 *
	 * Libera um resultado da memória
	 *
	 */
	public function free_result() {
		return mysqli_free_result($this->query);
	}

	/**
	 *
	 * Fecha a conexão MySQL
	 *
	 */
	public function disconnect() {
		return mysqli_close($this->connect);
	}

}

?>